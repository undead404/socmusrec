//асоціативний масив, щоб зберігати муз. виконавців
//window.targets = [];
window.lastfmKey = "2dcdc793309afa969dffb976eb7702d1";
//Алфавіт в капсі
window.alphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//Алфавіт не в капсі
window.alphaLower = window.alphaUpper.toLowerCase();
//Максимальна оброблювана кількість виконавців
window.ARTISTSLIMIT = 50;
//кількість тегів, що обробляються для кожного виконавця
window.TAGCOUNTLIMIT = 5;
//кількість улюблених тегів, які слід показати
window.TAGSNUMTOSHOW = 10;
window.vkApiId = 4988609;
//дивні відповіді
//window.strange = [];

function Target(num)
{
	setOperation('Создание объекта цели');
	var self = this;
	self.number = num;
	self.$container = $('#target-' + self.number);
	self.artists = Object.create(null);
	self.similarArtists = [];
	self.blockInput = function()
	{
		//log('Блокировка ввода');
		self.$container.find('input,button').attr('disabled', 'disabled');
		//log('Ввод заблокирован.');
	}
	self.unblockInput = function()
	{
		//log('Разблокировка ввода');
		self.$container.find('.target_id,.get_art_btn').removeAttr('disabled');
		if (self.artists && !$.isEmptyObject(self.artists))
			self.$container.find('.get_tags_btn,.get_rec_btn').removeAttr('disabled');
		//log('Ввод разблокирован.');
	};
	self.compareTo = function(other)
	{
		setOperation('Сравнение вкусов');
		if (isReadyToCompare())
		{
			var similarity = 0.0;
			var commonTags = Object.create(null);
			var tags1Sum = _(self.tags).values().reduce(function(memo, weight)
			{
				return memo + weight
			}, 0);
			var tags1RelWeights = Object.create(null);
			_(self.tags).forEach(function(weight, tag, tags)
			{
				tags1RelWeights[tag] = weight * 100 / tags1Sum;
			});
			var tags2Sum = _(other.tags).values().reduce(function(memo, weight)
			{
				return memo + weight
			}, 0);
			var tags2RelWeights = Object.create(null);
			_(other.tags).forEach(function(weight, tag, tags)
			{
				tags2RelWeights[tag] = weight * 100 / tags2Sum;
			});
			_(tags1RelWeights).forEach(function(relWeight, tag)
			{
				if ( tag in tags2RelWeights)
				{
					commonTags[tag] = (relWeight < tags2RelWeights[tag]) ? relWeight : tags2RelWeights[tag];
					similarity += commonTags[tag];
				}
			});
			window.commonTags = commonTags;
			var commonTagsString = _.chain(commonTags).pairs().sortBy(function(tagPair)
			{
				return -tagPair[1];
			}).map(function(tagPair)
			{
				return tagPair[0];
			}).slice(0, window.TAGCOUNTLIMIT).reduce(function(memo, tag, i)
			{
				return memo + ((i === 0) ? '' : ', ') + tag;
			}, '').value();
			$('#comparison').text('' + parseInt(similarity) + '% подібності: ' + commonTagsString);
		}
	};
	self.getFriends = function()
	{
		console.log('Requesting friends...');
		VK.Api.call('friends.get',
		{
			'fields' : 'can_see_audio,photo_50',
			'order' : 'name'
		}, self.saveFriends);
	};
	self.loadArtists = function()
	{
		setOperation('Получение исполнителей');
		self.blockInput();
		VK.Api.call("audio.get",
		{
			'owner_id' : self.vk_id
		}, self.saveAudios);
	}
	self.loadTags = function()
	{
		setOperation('Получение тегов');
		self.blockInput();
		self.tags = Object.create(null);
		var deferreds = [];
		_.chain(self.artists)
		//Виділення ключів (назв) з масиву виконавців
		.keys()
		//Отримання зрізу масиву від 0 до window.targets[0].artistsLIMIT елемента не
		// включно
		.slice(0, window.ARTISTSLIMIT).forEach(function(artist, i)
		{
			log('Запрос тегов для ' + artist);
			//додати до масиву відкладених надісланий ajax-запит
			deferreds.push($.ajax(
			{
				url : 'https://ws.audioscrobbler.com/2.0/',
				type : 'GET',
				dataType : 'json',
				data :
				{
					'api_key' : window.lastfmKey,
					'artist' : artist,
					'autocorrect' : 1,
					'format' : 'json',
					'method' : 'artist.gettoptags'
				},
			})
			//додати до ланцюжка і повернути новий відкладений
			.pipe(function(response)
			{
				log('Обработка тегов к ' + artist);
				//якщо у JSON-відповіді є ключ error
				if (response.error)
				{
					log(artist + ': ошибка ' + response.error);
					if (response.error == 6)
						delete self.artists[artist];
					return;
				}
				//якщо у JSON-відповіді є ключ toptags.#text
				if (response.toptags['#text'])
				{

					//window.strange.push(response);
					return;
				}
				//Обгорнути response.toptags.tag в underscore-об’єкт
				_(response.toptags.tag)
				//знайти перший елемент, для якого передана функція поверне true (насправді обхід
				// зрізу масива до елемента номер window.TAGCOUNTLIMIT)
				.find(function(tag)
				{
					if (!tag.name || tag['#text'] || tag.name == 'seen live')
					{
						//window.strange.push(response);
						return;
					}
					if (parseInt(tag.count) < window.TAGCOUNTLIMIT)
					{
						//console.log('tag ' + tag.name + ' is oops.');
						return true;
					}
					//якщо тег уже зустрічався
					if (tag.name in self.tags)
					{
						self.tags[tag.name] += tag.count * self.artists[artist];
					} else
					{
						//вага тега дорівнює count, помноженому на вагу виконавця
						self.tags[tag.name] = tag.count * self.artists[artist];
					}
					return false;
				});
				//блок виправлення назви виконавця
				if (response.toptags['@attr'] && response.toptags['@attr'].artist != artist)
				{
					log("Исправление " + artist + " в " + response.toptags['@attr'].artist);

					if (response.toptags['@attr'].artist in self.artists)
					{
						self.artists[response.toptags['@attr'].artist] += self.artists[artist];
					} else
					{
						self.artists[response.toptags['@attr'].artist] = self.artists[artist];
					}
					delete self.artists[artist];
				}
			}));
		});
		//повернути відкладення, що завершиться після завершення deferreds (котрі усі уже
		// запущені)
		return $.when.apply(null, deferreds)
		//після deferreds незалежно від успішності завершення виконати
		.always(function()
		{
			log('Сортировка и вывод тегов');
			//теги
			var tagPairs = _.chain(self.tags).pairs()
			/*.filter(function(tagPair)
			{
			return tagPair[1] > 1.0;
			})*/
			//відсортувати за спаданням ваги
			.sortBy(function(tagPair)
			{
				return -tagPair[1];
			}).value();
			self.tags = _(tagPairs).object();
			var tagsString = _(tagPairs).slice(0, window.TAGSNUMTOSHOW)
			/*.reduce(function(memo, tagPair)
			 {
			 return memo + '<li>' + tagPair[0] + '</li>';
			 }, '<ol>') + '</ol>';*/.reduce(function(memo, tagPair, i)
			{
				return memo + ((i == 0) ? '' : ', ') + tagPair[0];
			}, '');
			self.$container.find('.tags_list').html(tagsString).show();
			self.unblockInput();
			log('Теги предоставлены.')
			self.showArtists();
			refreshCompareBlock();
		});
	}
	self.reset = function()
	{
		//log('Обнуление цели #' + self.number);
		self.artists = Object.create(null);
		self.$container.find('.get_rec_btn').attr('disabled', 'disabled');
		self.$container.find('.get_tags_btn').attr('disabled', 'disabled');
		self.$container.find('.artists_list').text('');
		self.$container.find('.tags_list').hide();
		self.$container.find('.recommended_list').hide();
		self.unblockInput();
	}
	self.resolveLink = function()
	{
		setOperation('Обработка идентификатора');
		var ololo = self.linkRe.exec(self.$container.find('.target_id').val());
		if (!ololo[1] || ololo[1] == self.screen_name)
			return;
		self.screen_name = ololo[1];
		VK.Api.call('users.get',
		{
			'user_ids' : self.screen_name,
			'fields' : 'can_see_audio,photo_50'
		}, function(response)
		{
			if (response.error)
			{
				self.$container.find('.user-info').html('Произошла ошибка.');
				console.log(response.error);
				return;
			}
			//console.log(response);
			self.audiosAreSeen = response.response[0].can_see_audio ? true : false;
			self.vk_id = response.response[0].uid;
			self.first_name = response.response[0].first_name;
			self.last_name = response.response[0].last_name;
			self.photo_50 = response.response[0].photo_50;
			self.reset();
			if (self.audiosAreSeen)
			{
				self.$container.find('.get_art_btn').removeAttr('disabled');
			} else
			{
				self.$container.find('.btn').attr('disabled', 'disabled');
			}
			refreshCompareBlock();
			self.$container.find('.user-info').html('<img src="' + self.photo_50 + '"/>' + self.first_name + ' ' + self.last_name + (self.audiosAreSeen ? '' : '. Аудио недоступны.'));
			log('Идентификатор обработан.');
		});
	}
	self.saveAudios = function(response)
	{
		log('Обработка аудио');
		self.response = response;
		//якщо відповіді нема, пустий масив або 0 єдиний елемент масиву
		if (response.response === undefined || response.response.length == 0 || response.response[0] === 0)
		{
			self.$container.find('.artists_list').html("Аудио нет!");
			self.unblockInput();
		} else
		{
			//window.numOfAudios = response.response.length;
			_(response.response).forEach(function(audio, i)
			{
				if (audio.artist != undefined)
				{
					//нормалізувати назву виконавця
					//audio.artist = normalizeTitle(audio.artist);
					//якщо виконавець вже зустрічався
					if (audio.artist in self.artists)
					{
						self.artists[audio.artist] += response.response.length - i;
					} else
					{
						//вага виконавця дорівнює сумі номерів його аудіо, починаючи з найпершого
						// (найстаршого), нульового
						self.artists[audio.artist] = response.response.length - i;
					}
				}
			});
			self.showArtists();
			self.unblockInput();
			log('Аудио обработаны');
		}
	}
	self.saveFriends = function(response)
	{
		console.log('handling friends');
		window.response = response;
		self.friends = response.response;
		var friendsString = _(self.friends).reduce(function(memo, friend)
		{
			return memo + "<div class='friend'><img src='" + friend.photo_50 + "'/><a href='//vk.com/id" + friend.uid + "'>" + friend.first_name + " " + friend.last_name + "</a></div>";
		}, '');
		$('#friendlist').html(friendsString);
	}
	self.showArtists = function()
	{
		//log('Сортировка и вывод исполнителей');
		var artistPairs = _.chain(self.artists).pairs()
		//Сортувати за спаданням ваги
		.sortBy(function(artistPair)
		{
			return -artistPair[1];
		}).value();
		self.artists = _(artistPairs).object();
		var artistsString = _(artistPairs).slice(0, window.ARTISTSLIMIT)
		/*.reduce(function(memo, artistPair)
		 {
		 return memo + '<li>' + artistPair[0] + '</li>';
		 }, '<ol>') + '</ol>';*/.reduce(function(memo, artistPair, i)
		{
			return memo + ((i === 0) ? '' : ', ') + artistPair[0];
		}, '');
		self.$container.find('.artists_list').html(artistsString);
		//log('Исполнители предоставлены');
	}
	self.$container.find('.get_art_btn').click(function()
	{
		self.reset();
		self.loadArtists();
	});
	self.$container.find('.get_rec_btn').click(self.loadRecommended);
	self.$container.find('.get_tags_btn').click(self.loadTags);
	self.$container.find('.target_id').focusout(self.resolveLink);
	log('Объект цели создан.')
	return self;
}

Target.prototype.linkRe = /(?:(?:https?:)?\/\/(?:m\.)?vk\.com\/)?([^\?#]*)(?:\?.*)?(?:#.*)?/i;

function log(msg)
{
	$status.val(msg);
}

//нормалізація назви
/*function normalizeTitle(title)
 {
 //обрізати пробільні символи з кінців
 title = title.trim();
 //розескейпити службові символи
 title = _.unescape(title);
 var result = [];
 //розрізати на символи
 _(title.split('')).forEach(function(symbol)
 {
 //шукати в масиві великих літер англійського алфавіту
 var indexInAlpha = window.alphaUpper.indexOf(symbol);
 //якщо знайдено в масиві великих літер англійського алфавіту
 if (indexInAlpha !== -1)
 {
 //замінити малою літерою
 symbol = window.alphaLower[indexInAlpha];
 }
 result.push(symbol);
 });
 //склеїти в рядок і повернути
 return result.join('');
 }*/

function isReadyToCompare()
{
	return window.targets && window.targets[0] && window.targets[0].tags && window.targets[1] && window.targets[1].tags;
}

function refreshCompareBlock()
{
	if (!isReadyToCompare())
	{
		$('#compare_button').attr('disabled', 'disabled');
	} else
	{
		$('#compare_button').removeAttr('disabled');
	}
	$('#comparison').html('');
}

//скидання стану застосунку
function reset()
{
	setOperation('Сброс целей');
	_(window.targets).forEach(function(target)
	{
		target.reset()
	});
	log('Цели сброшены');
}

window.currentOperation = '';
function setOperation(msg)
{
	//$operation.val(msg + '...');
	window.currentOperation = msg;
}

//виконати після формування DOM
$(function()
{
	window.$status = $('#status');
	//ініціювати VK.Api
	VK.init(
	{
		'apiId' : window.vkApiId
	});
	//функція перевірки авторизації
	window.checkAuthInfo = function(response)
	{
		if (response.session)
		{
			window.user = new Target(1);
			$('#login_button').hide();
			//отримати масив параметрів URI
			var paramz = window.location.hash.substr(1).split('&');
			//ввести у #target-1 .target_id id поточного користувача
			$('#target-1 .target_id').val(response.session.mid);
			window.user.resolveLink();
			$('#get_friends_btn').removeAttr('disabled').click(window.user.getFriends);
		}
	}
	//залогінитись з правами 10
	//VK.Auth.login(window.checkAuthInfo, 2+8);
	//вказати кнопку логіну VK
	VK.UI.button('login_button');
});
