//асоціативний масив, щоб зберігати муз. виконавців
window.targets = [];
window.lastfmKey = "2dcdc793309afa969dffb976eb7702d1";
//Алфавіт в капсі
window.alphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//Алфавіт не в капсі
window.alphaLower = window.alphaUpper.toLowerCase();
//Максимальна оброблювана кількість виконавців
window.ARTISTSLIMIT = 50;
window.LISTENERSLIMIT = 100;
//кількість подібних виконавців, що обробляються для кожного виконавця
window.SIMILARPERARTIST = 10;
//кількість тегів, що обробляються для кожного виконавця
window.TAGCOUNTLIMIT = 5;
//кількість улюблених тегів, які слід показати
window.TAGSNUMTOSHOW = 10;
window.vkApiId = 4988609;
//дивні відповіді
//window.strange = [];

function Target(num)
{
	setOperation('Создание объекта цели');
	var self = this;
	self.number = num;
	self.$container = $('#target-' + self.number);
	self.artists = [];
	self.similarArtists = [];
	self.blockInput = function()
	{
		//log('Блокировка ввода');
		self.$container.find('input,button').attr('disabled', 'disabled');
		//log('Ввод заблокирован.');
	}
	self.unblockInput = function()
	{
		//log('Разблокировка ввода');
		self.$container.find('.target-id').removeAttr('disabled');
		if (self.can_see_audio)
		{
			self.$container.find('.get_art_btn').removeAttr('disabled');
			if (self.artists && self.artists.length)
			{
				self.$container.find('.get_tags_btn,.get_rec_btn').removeAttr('disabled');
			}
		}

		//log('Ввод разблокирован.');
	}
	self.correctArtist = function(artist)
	{
		log("Запрос исправлений по поводу \"" + artist.name + "\"");
		return $.ajax(
		{
			url : 'https://ws.audioscrobbler.com/2.0/',
			type : 'GET',
			dataType : 'json',
			data :
			{
				'api_key' : window.lastfmKey,
				'artist' : artist.name,
				'autocorrect' : 1,
				'format' : 'json',
				'method' : 'artist.getinfo'
			},
		}).pipe(function(response)
		{
			log("Корректировка исполнителя " + artist.name);
			if (response.error)
			{
				if (response.error == 6)
				{
					log("Исполнитель \"" + artist.name + "\" не существует.")
					self.artists = _(self.artists).without(artist);
				}
				return;
			}
			if (response.artist.name != artist.name)
			{
				log(artist.name + " исправляется в " + response.artist.name);
				var correctArtist = _(self.artists).find(function(a)
				{
					return a.name == response.artist.name;
				});
				if (correctArtist)
				{
					self.artists = _(self.artists).without(artist);
					//console.log('Исправленный встречался!');
					correctArtist.weight += artist.weight;
					if (!_(correctArtist).has('url'))
					{
						//console.log('Исправленный корректен!');
						artist = correctArtist;
					} else
					{
						return;
					}

				} else
				{
					//console.log('Исправленный не встречался...');
					artist.name = response.artist.name;
				}
			}
			var numOfListeners = parseInt(response.artist.stats.listeners);
			if (numOfListeners < window.LISTENERSLIMIT)
			{
				log("Исполнитель \"" + artist.name + '" сомнителен.');
				self.artists = _(self.artists).without(artist);
			} else
			{
				log(artist.name + ' - OK!');
				_(artist).extend(response.artist);
				if (numOfListeners < 10000)
				{
					artist.isUnderground = true;
				} else
				{
					if (numOfListeners > 1000000)
					{
						artist.isMainstream = true;
					}
				}
				self.numOfCorrectArtists++;
			}

		});
	}
	self.correctArtists = function()
	{
		setOperation('Исправление названий исполнителей');
		if (self.numOfCorrectArtists === undefined)
			self.numOfCorrectArtists = 0;
		var deferreds = [];
		if (self.numOfCorrectArtists < window.ARTISTSLIMIT && _(self.artists).size() > self.numOfCorrectArtists)
		{
			_.chain(self.artists).slice(self.numOfCorrectArtists, window.ARTISTSLIMIT).forEach(function(artist)
			//_.chain(self.artists).forEach(function(weight,artist)
			{
				deferreds.push(self.correctArtist(artist));
			});

			return $.when.apply(null, deferreds).then(function()
			{
				self.sortArtists();
				clearStatus();
			}).pipe(self.correctArtists);
		}
	}
	self.initHandlers = function()
	{
		
		self.$container.find('.get_art_btn').click(function()
		{
			self.reset();
			self.loadAudios();
		});
		self.$container.find('.get_rec_btn').click(self.loadSimilars);
		self.$container.find('.get_tags_btn').click(self.loadTags);
		self.$container.find('.target-id').focusout(self.resolveLink);
	};
	self.loadAudios = function()
	{
		setOperation('Получение исполнителей');
		log("Отправка запроса");
		self.blockInput();
		VK.Api.call("audio.get",
		{
			'owner_id' : self.uid
		}, self.saveAudios);
	};
	self.loadSimilars = function()
	{
		setOperation('Получение рекомендаций');
		self.blockInput();
		self.similars = Object.create(null);
		//масив відкладених - обробки надісланих запитів щодо рекомендованих
		var deferreds = [];
		var i = 0;
		//виділення ключів з асоц. масиву window.targets[0].artists, отримання зрізу
		// перших ARTISTSLIMIT виконавців, і для кожного з них - створення запиту до
		// ws.audioscrobbler.com/2.0
		_.chain(self.artists).slice(0, window.ARTISTSLIMIT).forEach(function(artist)
		{
			log('Запрос: ' + artist.name);
			deferreds.push($.ajax(
			{
				url : 'https://ws.audioscrobbler.com/2.0/',
				type : 'GET',
				dataType : 'json',
				data :
				{
					'api_key' : window.lastfmKey,
					'artist' : artist.name,
					'autocorrect' : 0,
					'format' : 'json',
					'limit' : window.SIMILARPERARTIST,
					'method' : 'artist.getsimilar'
				},
			})
			//після виконання відкладеного виконати над відповіддю наступне:
			.pipe(function(response)
			{
				log('Обработка: ' + artist.name);
				//якщо немає подібних або у відданому JSON є ключ '#text'
				if (!response.similarartists || response.similarartists['#text'])
				{
					console.log('no similars!');
					//window.strange.push(response);
					return;
				}
				//для кожного виконавця у низці за ключем similarartists.artist з JSON-відповіді
				_(response.similarartists.artist).forEach(function(similarArtist)
				{
					//якщо в виконавця не вказано імені або вказано ключ '#text'
					if (!similarArtist.name || similarArtist['#text'])
					{
						console.log('strange response!');
						//window.strange.push(response);
						return;
					}
					//якщо виконавця немає серед відомих оброблюваному користувачу
					if (!(_(self.artists).find(function(artist)
						{
							return artist.name == similarArtist.name;
						})))
					{
						//якщо уже є серед знайдених подібних
						if (similarArtist.name in self.similars)
						{
							//додати вагу відомого користувачу виконавця помножену на коефіцієнт подібності
							self.similars[similarArtist.name] += similarArtist.match * artist.weight;
						} else
						{
							//вага відомого користувачу виконавця помножена на коефіцієнт подібності
							self.similars[similarArtist.name] = similarArtist.match * artist.weight;
						}
					} else
					{
						console.log(similarArtist.name + ' уже знайомий.');
					}
				});
				//якщо у відповіді є ключ similarartists.@attr і за ключем
				// similarartists.@attr.artist не такий же, як відомий користувачу виконавець -
				// виправлення назви виконавця у низці window.targets[0].artists
				/*if (response.similarartists['@attr'] && response.similarartists['@attr'].artist
				 * != artist)
				 {
				 log('Исправление названия ' + artist + ' в ' +
				 response.similarartists['@attr'].artist);
				 //якщо в window.targets[0].artists є similarartists.@attr.artist з відповіді
				 if (response.similarartists['@attr'].artist in self.artists)
				 {
				 self.artists[response.similarartists['@attr'].artist] += self.artists[artist];
				 } else
				 {
				 self.artists[response.similarartists['@attr'].artist] = self.artists[artist];
				 }
				 delete self.artists[artist];
				 }*/
			}));
			i++;
			return false;
		});
		//повернути відкладення, складене з очікування deferreds відкладень - надісланих
		// до ws.audioscrobbler.com/2.0 запитів, та їх обробки
		return $.when.apply(null, deferreds)
		//наступне виконати після закінчення усіх deferreds в будь-якому випадку
		.always(function()
		{
			log('Сортировка и вывод рекомендаций');
			//перетворення асоц. масиву на пари [ключ, значення], видалення пар, де значення
			// менше або дорівнює 1.0, сортування за спаданням значення
			var similarsPairs = _.chain(self.similars).pairs().filter(function(recPair)
			{
				return recPair[1] > 1.0;
			}).sortBy(function(recPair)
			{
				return -recPair[1];
			}).value();
			//перетворення таких пар назад на асоц. масив
			self.similars = _(similarsPairs).object();
			//згортання - reduce
			var recsString = _(similarsPairs).reduce(function(memo, recPair)
			{
				return memo + '<li>' + recPair[0] + '</li>';
			}, '<ol>') + '</ol>';
			console.log(recsString);
			//викласти ol список рекомендованих виконавців у #target-1 .similars_list
			self.$container.find('.similars_list').html(recsString).show();
			self.unblockInput();
			clearStatus();
			//self.showArtists();
		});
	}
	self.loadTags = function()
	{
		setOperation('Получение тегов');
		self.blockInput();
		self.tags = Object.create(null);
		var deferreds = [];
		_.chain(self.artists)
		//Отримання зрізу масиву від 0 до window.targets[0].artistsLIMIT елемента не
		// включно
		/*.slice(0, window.ARTISTSLIMIT)*/.forEach(function(artist, i)
		{
			log('Запрос: ' + artist.name);
			//додати до масиву відкладених надісланий ajax-запит
			deferreds.push($.ajax(
			{
				url : 'https://ws.audioscrobbler.com/2.0/',
				type : 'GET',
				dataType : 'json',
				data :
				{
					'api_key' : window.lastfmKey,
					'artist' : artist.name,
					'autocorrect' : 0,
					'format' : 'json',
					'method' : 'artist.gettoptags'
				},
			})
			//додати до ланцюжка і повернути новий відкладений
			.pipe(function(response)
			{
				log('Обработка: ' + artist.name);
				//якщо у JSON-відповіді є ключ toptags.#text
				if (response.error || response.toptags['#text'])
				{

					//window.strange.push(response);
					return;
				}
				//Обгорнути response.toptags.tag в underscore-об’єкт
				_(response.toptags.tag)
				//знайти перший елемент, для якого передана функція поверне true (насправді обхід
				// зрізу масива до елемента номер window.TAGCOUNTLIMIT)
				.find(function(tag)
				{
					if (!tag.name || tag['#text'] || tag.name == 'seen live')
					{
						//window.strange.push(response);
						return;
					}
					if (parseInt(tag.count) < window.TAGCOUNTLIMIT)
					{
						//console.log('tag ' + tag.name + ' is oops.');
						return true;
					}
					//якщо тег уже зустрічався
					if (tag.name in self.tags)
					{
						self.tags[tag.name] += tag.count * artist.weight;
					} else
					{
						//вага тега дорівнює count, помноженому на вагу виконавця
						self.tags[tag.name] = tag.count * artist.weight;
					}
					return false;
				});
				//блок виправлення назви виконавця
				/*if (response.toptags['@attr'] && response.toptags['@attr'].artist != artist)
				 {
				 log("Исправление " + artist + " в " + response.toptags['@attr'].artist);

				 if (response.toptags['@attr'].artist in self.artists)
				 {
				 self.artists[response.toptags['@attr'].artist] += self.artists[artist];
				 } else
				 {
				 self.artists[response.toptags['@attr'].artist] = self.artists[artist];
				 }
				 delete self.artists[artist];
				 }*/
			}));
		});
		//повернути відкладення, що завершиться після завершення deferreds (котрі усі уже
		// запущені)
		return $.when.apply(null, deferreds)
		//після deferreds незалежно від успішності завершення виконати
		.always(function()
		{
			log('Сортировка и вывод тегов');
			//теги
			var tagPairs = _.chain(self.tags).pairs()
			/*.filter(function(tagPair)
			{
			return tagPair[1] > 1.0;
			})*/
			//відсортувати за спаданням ваги
			.sortBy(function(tagPair)
			{
				return -tagPair[1];
			}).value();
			self.tags = _(tagPairs).object();
			var tagsString = _(tagPairs).slice(0, window.TAGSNUMTOSHOW)
			/*.reduce(function(memo, tagPair)
			 {
			 return memo + '<li>' + tagPair[0] + '</li>';
			 }, '<ol>') + '</ol>';*/.reduce(function(memo, tagPair, i)
			{
				return memo + ((i == 0) ? '' : ', ') + tagPair[0];
			}, '');
			self.$container.find('.tags_list').html(tagsString).show();
			self.unblockInput();
			clearStatus();
			//self.showArtists();
			refreshCompareBlock();
		});
	}
	self.reset = function()
	{
		//log('Обнуление цели #' + self.number);
		self.artists = [];
		self.tags = self.similars = null;
		self.$container.find('.get_rec_btn').attr('disabled', 'disabled');
		self.$container.find('.get_tags_btn').attr('disabled', 'disabled');
		self.$container.find('.artists_list').text('');
		self.$container.find('.tags_list').hide();
		self.$container.find('.similars_list').hide();
		self.numOfCorrectArtists = 0;
		self.unblockInput();
	}
	self.resolveId = function(id)
	{
		VK.Api.call('users.get',
		{
			'user_ids' : id,
			'fields' : 'can_see_audio,photo_50'
		}, function(response)
		{
			if (response.error)
			{
				self.$container.find('.target-info').html('Произошла ошибка.');
				console.log(response.error);
				self.unblockInput();
				return;
			}
			//console.log(response);
			_(self).extend(response.response[0]);
			if (self.can_see_audio)
			{
				self.$container.find('.get_art_btn').removeAttr('disabled');
			} else
			{
				self.$container.find('.btn').attr('disabled', 'disabled');
			}
			refreshCompareBlock();
			var userInfo = '<a href="//vk.com/id' + self.uid + '" target="__blank"><img src="' + self.photo_50 + '"/>' + self.first_name + ' ' + self.last_name + '</a>' + (self.can_see_audio ? '' : '. Аудио недоступны.');
			var session = VK.Auth.getSession();
			if (session && self.uid == session.mid)
			{
				$('.user-info').html(userInfo);
			}
			self.$container.find('.target-info').html(userInfo);
			self.$container.show();
			clearStatus();
			self.unblockInput();
		});
	}
	self.resolveLink = function()
	{
		setOperation('Обработка идентификатора');
		self.blockInput();
		var ololo = self.linkRe.exec(self.$container.find('.target-id').val());
		if (!ololo[1] || ololo[1] == self.screen_name)
		{
			self.unblockInput();
			return;
		}
		self.screen_name = ololo[1];
		self.reset();
		refreshCompareBlock();
		self.resolveId(self.screen_name);
	}
	self.saveArtist = function(response)
	{
		//log("Корректировка исполнителя " + artist);
		if (response.error)
		{
			if (response.error == 6)
			{
				log("Исполнитель  не существует.");
			}
			return;
		}

		if (parseInt(response.artist.stats.listeners) < window.LISTENERSLIMIT)
		{
			log("Исполнитель \"" + response.artist.name + '" сомнителен.');
		} else
		{
			log(response.artist.name + ' - OK!');
			self.artists.push(response.artist);
			self.numOfCorrectArtists++;
		}

	}
	self.saveArtists = function()
	{
		if (!self.audios || !self.audios.length)
			return;
		self.audios = _(self.audios).sortBy(function(audio)
		{
			return audio.artist;
		});
		_(self.audios).forEach(function(audio)
		{
			if (audio.artist == undefined)
			{
				return;
			}
			audio.artist = audio.artist.trim();
			var artist = _(self.artists).find(function(artist)
			{
				return artist.name == audio.artist;
			});
			if (artist)
			{
				artist.weight += audio.weight;
			} else
			{
				self.artists.push(
				{
					'name' : audio.artist,
					'weight' : audio.weight
				});
			}
		});
		self.sortArtists();
	};
	self.saveAudios = function(response)
	{
		log('Обработка аудио');
		//self.response = response;
		//якщо відповіді нема, пустий масив або 0 єдиний елемент масиву
		if (response.response === undefined || response.response.length == 0 || response.response[0] === 0)
		{
			self.$container.find('.artists_list').html("Аудио нет!");
			self.unblockInput();
		} else
		{
			self.audios = response.response;
			//window.numOfAudios = response.response.length;
			_(self.audios).forEach(function(audio, i, audios)
			{
				audio.weight = audios.length - i;
			});
			self.saveArtists();
			self.correctArtists().then(function()
			{
				self.showArtists();
				self.unblockInput();
				clearStatus();
			});
		}
	}
	self.showArtists = function()
	{
		var artistsString = _.chain(self.artists).slice(0, window.ARTISTSLIMIT).reduce(function(memo, artist, i)
		{
			if (artist.isUnderground)
			{
				return memo + ((i === 0) ? '' : ', ') + "<span class='underground' title='underground'>" + artist.name + "</span>";
			} else
			{
				if (artist.isMainstream)
				{
					return memo + ((i === 0) ? '' : ', ') + "<span class='mainstream' title='mainstream'>" + artist.name + "</span>";
				} else
				{
					return memo + ((i === 0) ? '' : ', ') + "<span>" + artist.name + "</span>";
				}
			}
		}, '').value();
		self.$container.find('.artists_list').html(artistsString);
		self.$container.find('.underground,.mainstream').tooltip();
		//log('Исполнители предоставлены');
	}
	self.sortArtists = function()
	{
		self.artists = _(self.artists).sortBy(function(artist)
		{
			return -artist.weight;
		});
	};
	self.initHandlers();
	log('Объект цели создан.')
	return self;
}

Target.prototype.linkRe = /(?:(?:https?:)?\/\/(?:m\.)?vk\.com\/)?([^\?#]*)(?:\?.*)?(?:#.*)?/i;

function clearStatus()
{
	window.currentOperation = '';
	$status.val('');
}

function compareTargets()
{
	setOperation('Сравнение');
	log('Сравнение вкусов');
	if (isReadyToCompare())
	{
		var similarity = 0.0;
		var commonTags = Object.create(null);
		var tags1Sum = _(window.targets[0].tags).values().reduce(function(memo, weight)
		{
			return memo + weight
		}, 0);
		var tags1RelWeights = Object.create(null);
		_(window.targets[0].tags).forEach(function(weight, tag, tags)
		{
			tags1RelWeights[tag] = weight * 100 / tags1Sum;
		});
		var tags2Sum = _(window.targets[1].tags).values().reduce(function(memo, weight)
		{
			return memo + weight
		}, 0);
		var tags2RelWeights = Object.create(null);
		_(window.targets[1].tags).forEach(function(weight, tag, tags)
		{
			tags2RelWeights[tag] = weight * 100 / tags2Sum;
		});
		_(tags1RelWeights).forEach(function(relWeight, tag)
		{
			if ( tag in tags2RelWeights)
			{
				commonTags[tag] = (relWeight < tags2RelWeights[tag]) ? relWeight : tags2RelWeights[tag];
				similarity += commonTags[tag];
			}
		});
		window.commonTags = commonTags;
		var commonTagsString = _.chain(commonTags).pairs().sortBy(function(tagPair)
		{
			return -tagPair[1];
		}).map(function(tagPair)
		{
			return tagPair[0];
		}).slice(0, window.TAGCOUNTLIMIT).reduce(function(memo, tag, i)
		{
			return memo + ((i === 0) ? '' : ', ') + tag;
		}, '').value();
		$('#comparison-result').text('' + parseInt(similarity) + '% подібності: ' + commonTagsString);
	};
	clearStatus();
}

function log(msg)
{
	console.log(window.currentOperation + ': ' + msg);
	$status.val(window.currentOperation + ': ' + msg);
}

//нормалізація назви
/*function normalizeTitle(title)
 {
 //обрізати пробільні символи з кінців
 title = title.trim();
 //розескейпити службові символи
 title = _.unescape(title);
 var result = [];
 //розрізати на символи
 _(title.split('')).forEach(function(symbol)
 {
 //шукати в масиві великих літер англійського алфавіту
 var indexInAlpha = window.alphaUpper.indexOf(symbol);
 //якщо знайдено в масиві великих літер англійського алфавіту
 if (indexInAlpha !== -1)
 {
 //замінити малою літерою
 symbol = window.alphaLower[indexInAlpha];
 }
 result.push(symbol);
 });
 //склеїти в рядок і повернути
 return result.join('');
 }*/

function isReadyToCompare()
{
	return window.targets && window.targets[0] && window.targets[0].tags && !$.isEmptyObject(window.targets[0].tags) && window.targets[1] && window.targets[1].tags && !$.isEmptyObject(window.targets[1].tags);
}

function refreshCompareBlock()
{
	if (!isReadyToCompare())
	{
		$('#compare_button').attr('disabled', 'disabled');
	} else
	{
		$('#compare_button').removeAttr('disabled');
	}
	$('#comparison-result').html('');
}

//скидання стану застосунку
function reset()
{
	_(window.targets).forEach(function(target)
	{
		target.reset()
	});
}

window.currentOperation = '';
function setOperation(msg)
{
	//$operation.val(msg + '...');
	window.currentOperation = msg;
}

//виконати після формування DOM
$(function()
{
	window.$status = $('#status');
	//ініціювати VK.Api
	VK.init(
	{
		'apiId' : window.vkApiId
	});
	//функція перевірки авторизації
	window.checkAuthInfo = function(response)
	{
		if (response.session)
		{
			window.user = new Target('user');
			window.targets[0] = new Target(1);
			window.targets[1] = new Target(2);
			$('#login_button').hide();
			//отримати масив параметрів URI
			var paramz = window.location.hash.substr(1).split('&');
			//ввести у #target-1 .target-id id поточного користувача
			$('#target-user .target-id').val(response.session.mid);
			//window.targets[0].resolveLink();
			window.user.resolveId(response.session.mid);
			$('#compare_button').click(compareTargets);
			/*$('#main-nav [href=#comparison]').click(function(e)
			{
				if (!window.targets[0].$container.find('.target-id').val())
				{
					window.targets[0].$container.html(window.user.$container.html());
					_(window.targets[0]).extend(window.user);
					window.targets[0].initHandlers();
				}
			});*/
			$('#main-nav a').click(function(e)
			{
				e.preventDefault();
				$(this).tab('show');
			})
		}
	}
	//залогінитись з правами 10
	//VK.Auth.login(window.checkAuthInfo, 2+8);
	//вказати кнопку логіну VK
	VK.UI.button('login_button');
});
