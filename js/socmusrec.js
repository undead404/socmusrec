"use strict";
var socmusrecApp = angular.module('socmusrecApp', ['ngRoute'])
    .config(function ($sceProvider, $routeProvider)
    {
		$routeProvider.when('/about',
        {
            templateUrl: 'views/about.html',
            controller: 'AboutCtrl'
        });
        $routeProvider.when('/comparison',
        {
            templateUrl: 'views/comparison.html',
            controller: 'ComparisonCtrl'
        });
        $routeProvider.when('/login',
        {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl'
        });
        $routeProvider.otherwise(
        {
            redirectTo: '/about'
        });
        $sceProvider.enabled(false);
    })
    .run(function ($rootScope, $http,$location)
    {
		$rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
			$rootScope.page=$location.path();
		    //console.log('Current route name: ' + $location.path());
   		});
		$rootScope.busy=false;
        $rootScope.lastfmKey = "2dcdc793309afa969dffb976eb7702d1";
        //Алфавіт в капсі
        $rootScope.alphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //Алфавіт не в капсі
        $rootScope.alphaLower = $rootScope.alphaUpper.toLowerCase();
        $rootScope.completeRequestsNum = 0;
        $rootScope.getArtistByName = function (artistName)
        {
            return $http(
            {
                'method': 'GET',
                'url': 'https://ws.audioscrobbler.com/2.0/',
                params:
                {
                    'api_key': $rootScope.lastfmKey,
                    'artist': artistName,
                    'autocorrect': 1,
                    'format': 'json',
                    'lang': 'ru',
                    'method': 'artist.getinfo'
                }
            });
        };
        $rootScope.log = function (logMessage)
        {
            //console.log(message);
            $rootScope.logMessages.push(logMessage);
            $rootScope.appStatus = logMessage.message;
        };
        $rootScope.logMessages = [];
        $rootScope.appStatus = "";
        //кількість улюблених тегів, які слід показати
        $rootScope.totalRequestsNum = 0;
        $rootScope.userId = 0;
        $rootScope.vkApiId = 4988609;
        //дивні відповіді
        //$rootScope.strange = [];
    });
