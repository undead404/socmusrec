"use strict";

function declOfNum(number, titles)
{
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : declOfNum.cases[(number % 10 < 5) ? number % 10 : 5]];
}
declOfNum.cases = [2, 0, 1, 1, 1, 2];
window.minutesTitles = ["минуту", "минуты", "минут"];
window.secondsTitles = ["секунду", "секунды", "секунд"];
socmusrecApp.controller('ComparisonCtrl', function ComparisonCtrl($scope, $location, $q, $rootScope)
{
    //console.log($rootScope.userId);
    //$scope.avto=true;
    if (!$rootScope.userId)
    {
        $location.path('/login');
    }
    $scope.areTargetsPresent = function ()
    {
        //console.log('$scope.areTargetsPresent()');
        return $scope.targets[0].can_see_audio && $scope.targets[1].can_see_audio;
    };
    $scope.ARTISTSLIMIT = 50;
    var beginTime = undefined;
    $scope.bgImage = undefined;
    $scope.calcCommonArtists = function ()
    {
        var foundArtist;
        //var commonArtistsObj = Object.create(null);
        $scope.commonArtists = [];
        var targetA, targetB;
        if ($scope.targets[0].artists.length > $scope.targets[1].artists.length)
        {
            targetA = $scope.targets[1];
            targetB = $scope.targets[0];
        }
        else
        {
            targetA = $scope.targets[0];
            targetB = $scope.targets[1];
        }
        _(targetA.artists)
            .forEach(function (artist)
            {
                foundArtist = _(targetB.artists)
                    .findWhere(
                    {
                        'name': artist.name
                    });
                if (foundArtist)
                {
                    //console.log(artist, foundArtist);
                    /*commonArtistsObj[artist.name] = artist.weight < foundArtist.weight ? artist.weight :
                        foundArtist.weight;*/
                    $scope.commonArtists.push(
                    {
                        'artist': artist,
                        'weight': artist.weight < foundArtist.weight ? artist.weight : foundArtist
                            .weight
                    });
                }
            });
        window.commonArtists = $scope.commonArtists;
        if (commonArtists.length)
        {
            $scope.commonArtists = _($scope.commonArtists)
                .sortBy(function (commonArtist)
                {
                    return -commonArtist.weight;
                });
            $scope.bgImage = $scope.commonArtists[0].artist.image;
        }
    }
    $scope.compare = function ()
    {
        //console.log('$scope.compare()');
        if (!$scope.isReady())
        {
            $rootScope.log(
            {
                'message': "Цели сравнения не готовы",
                'type': 'danger'
            });
            return $q.reject();
        }
        var similarity = 0.0;
        var commonTags = Object.create(null);
        //var tags1Sum=$scope.targets[0].getTagsWeightSum();
        var tags1RelWeights = window.tags1RelWeights = $scope.targets[0].getTagsRelWeights();
        //var tags2Sum=$scope.targets[1].getTagsWeightSum();
        var tags2RelWeights = window.tags2RelWeights = $scope.targets[1].getTagsRelWeights();
        _(tags1RelWeights)
            .forEach(function (relWeight, tag)
            {
                if (tag in tags2RelWeights)
                {
                    commonTags[tag] = (relWeight < tags2RelWeights[tag]) ? relWeight : tags2RelWeights[
                        tag];
                    similarity += commonTags[tag];
                }
            });
        $scope.commonTags = window.commonTags = commonTags;
        var commonTagsString = _.chain(commonTags)
            .pairs()
            .sortBy(function (tagPair)
            {
                return -tagPair[1];
            })
            .map(function (tagPair)
            {
                return tagPair[0];
            })
            .slice(0, $scope.TAGSNUMTOSHOW)
            .filter(function (tagName)
            {
                for (var i = 0; i < $scope.filterFromTags.length; i++)
                {
                    if (tagName.indexOf($scope.filterFromTags[i]) !== -1) return false;
                }
                return true;
            })
            .value()
            .join(', ');
        $scope.calcCommonArtists();
        $scope.similarity = window.similarity = {
            'artistsString': _.chain($scope.commonArtists)
                .map(function (commonArtist)
                {
                    return commonArtist.artist.name;
                })
                .value()
                .join(', '),
            'description': commonTagsString ? commonTagsString : '...',
            'quantity': Math.trunc(similarity)
        };
        var secondsPassed = parseInt((new Date() - $scope.begin) / 1000);
        var minutesPassed = parseInt(secondsPassed / 60);
        secondsPassed %= 60;
        /*var hoursPassed=minutesPassed/60;
        minutesPassed%=60;*/
        $rootScope.log(
        {
            'message': "Сравнение завершено за " + minutesPassed + " " + declOfNum(minutesPassed,
                window.minutesTitles) + " " + secondsPassed + " " + declOfNum(secondsPassed,
                window.secondsTitles) + ".",
            'type': 'success'
        });
        $scope.done = true;
    };
    $scope.done = false;
    $scope.filterFromTags = ['gavno', 'govno', 'pizd', 'shit', 'anus', 'gay','tuz'];
    $scope.isLogShown = false;
    $scope.isReady = function ()
    {
        //console.log('$rootScope.isReady()');
        return $scope.targets[0].isReady() && $scope.targets[1].isReady();
    };
    $scope.LISTENERSLIMIT = 100;
    $scope.resetResult = function ()
    {
        $scope.done = false;
        $scope.similarity = null;
        $scope.bgImage = undefined;
    };
    $scope.similarity = null;
    $scope.startComparison = function ()
    {
        //console.log('$scope.startComparison()');
        if (!$scope.targets[0].uid || !$scope.targets[1].uid)
        {
            $rootScope.log(
            {
                'message': "Цель не указана",
                'type': 'danger'
            });
            return $q.reject();
        }
        $scope.begin = new Date();
        $rootScope.busy = true;
        $rootScope.totalRequestsNum = 0;
        $rootScope.completeRequestsNum = 0;
        $scope.similarity = null;
        var task1;
        if (!$scope.targets[0].tags || $scope.targets[0].tags.length < 1)
        {
            task1 = $scope.targets[0].loadEverything();
        }
        else task1 = $q.defer();
        var task2;
        if (!$scope.targets[1].tags || $scope.targets[1].tags.length < 1)
        {
            task2 = $scope.targets[1].loadEverything();
        }
        else task2 = $q.defer();
        return $q.all([
                task1,
                task2
            ])
            .then($scope.compare)
            .finally(function ()
            {
                $rootScope.busy = false;
                $scope.begin = undefined;
            });
    };
    $scope.TAGSNUMTOSHOW = 10;
    $scope.targets = [];
});
