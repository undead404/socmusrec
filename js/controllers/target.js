"use strict";
socmusrecApp.controller('TargetCtrl', function TargetCtrl($scope, $rootScope, $q, $timeout)
{
    $scope.artists = [];
    var audiosNum = 0;
    $scope.bgImage = undefined;
    var calcLevel = function ()
    {
        var imaginedWeight = 0;
        var level = 0;
        while (imaginedWeight < $scope.artists[0].weight)
        {
            level++;
            imaginedWeight += audiosNum + level;
        }
        $scope.level = level;
    }
    $scope.getTagsRelWeights = function ()
    {
        //console.log('$scope.getTagsRelWeights()');
        var tagsRelWeights = Object.create(null);
        var tagsWeightSum = getTagsWeightSum();
        _($scope.tags)
            .forEach(function (tag, i, tags)
            {
                //console.log(weight,tag);
                tagsRelWeights[tag.name] = tag.weight * 100 / tagsWeightSum;
                //console.log(tagsRelWeights[tag],tag);
            });
        return tagsRelWeights;
    };
    var getTagsWeightSum = function ()
    {
        //console.log('getTagsWeightSum()');
        return _($scope.tags)
            .reduce(function (memo, tag, i)
            {
                //console.log(weight,i);
                return memo + tag.weight;
            }, 0);
    };
    $scope.handleInput = function ()
    {
        //console.log('$scope.handleInput()');
        inputDeferreds.push($timeout(function () {}, 1000));
        if (!$scope.isReceivingInput)
        {
            $scope.isReceivingInput = true;
            $q.all(inputDeferreds)
                .then(function ()
                {
                    $scope.isReceivingInput = false;
                    inputDeferreds = [];
                    $scope.resolveUid();
                });
        }
    };
    $scope.inputId = "";
    var inputDeferreds = [];
    $scope.isReady = function ()
    {
        //console.log('$scope.isReady()');
        //console.log($scope.can_see_audio, $scope.tags);
        return $scope.can_see_audio && $scope.tags && $scope.tags.length;
    };
    $scope.isReceivingInput = false;
    $scope.level = 0;
    var linkRe = /(?:(?:https?:)?\/\/(?:m\.)?vk\.com\/)?([^\?#]*)(?:\?.*)?(?:#.*)?/i;
    var loadAudios = function ()
    {
        //console.log('loadAudios()');
        //$scope.busy=true;
        var deferred = $q.defer();
        //window.deferred=deferred;
        //console.log($scope.uid);
        //console.log("VK.Api.call('audio.get',{'owner_id' : "+$scope.uid+"}, "+deferred.resolve+")");
        VK.Api.call("audio.get",
        {
            'owner_id': $scope.uid
        }, function (response)
        {
            $scope.response = response;
            deferred.resolve(response);
        });
        $rootScope.totalRequestsNum++;
        $rootScope.log(
        {
            'message': $scope.first_name + ' ' + $scope.last_name + ' - запрошены аудио',
            'type': 'info'
        });
        return deferred.promise;
    };
    var loadArtistInfo = function (artist)
    {
        //console.log('requestArtistInfo()');
        $rootScope.totalRequestsNum++;
        /*$rootScope.log({
        	'message':'Запрошена информация об исполнителе "'+artist.name+'"',
        	'type':'info'
        });*/
        return $rootScope.getArtistByName(artist.name)
            .then(function (response)
            {
                saveArtistInfo(response, artist);
            });
    };
    var loadArtistInfos = function ()
    {
        //console.log('requestArtistCorrections()');
        var deferreds = [];
        _.chain($scope.artists)
            .forEach(function (artist)
                //_.chain($scope.artists).forEach(function(weight,artist)
                {
                    deferreds.push(loadArtistInfo(artist));
                });
        return $q.all(deferreds)
            .then(function ()
            {
                if ($scope.artists.length == 0)
                {
                    $scope.artistsString = "<span class='label label-danger'>Исполнителей нет!</span>";
                    $scope.level = 1;
                    $rootScope.log(
                    {
                        'message': "У цели нет исполнителей в аудио",
                        'type': 'danger'
                    });
                    return $q.reject();
                }
            });
    };
    $scope.loadEverything = function ()
    {
        //console.log('$scope.loadEverything()');
        return loadAudios()
            .then(saveAudios)
            .then(saveArtists)
            //.then(requestArtistCorrections)
            .then(loadArtistInfos)
            .then(sortArtists)
            .then(showArtists)
            .then(setBgImage)
            .then(calcLevel)
            .then(saveTags)
            //.then(loadTags)
            //.then(saveTags)
            .then(showTags);
    };
    var reset = function ()
    {
        //console.log('reset()');
        $scope.artists = [];
        $scope.artistsString = '';
        $scope.bgImage = undefined;
        $scope.level = 0;
        $scope.tags = [];
        $scope.tagsString = '';
        $scope.numOfCorrectArtists = 0;
        $rootScope.logMessages = [];
        $scope.resetResult();
        $scope.uid = undefined;
    };
    $scope.resolveUid = function ()
    {
        //console.log('$scope.resolveUid()');
        //$scope.$apply();
        var ololo = linkRe.exec($scope.inputId);
        if (!ololo[1] || ololo[1] == screenName)
        {
            return;
        }
        //$rootScope.busy=true;
        $rootScope.totalRequestsNum++;
        screenName = ololo[1];
        reset();
        VK.Api.call('users.get',
        {
            'user_ids': screenName,
            'fields': 'can_see_audio,photo_50'
        }, saveUid);
        $rootScope.log(
        {
            'message': 'Запрошена информация о пользователе ' + screenName + '',
            'type': 'info'
        });
    };
    var saveArtistInfo = function (response, artist)
    {
        //console.log('saveArtistInfo('+response+','+artist+')');
        $rootScope.completeRequestsNum++;
        //window.response=response;
        //window.artist=artist;
        //throw new Exception();
        var response = response.data;
        if (response.error)
        {
            if (response.error == 6)
            {
                $rootScope.log(
                {
                    'message': "Исполнитель \"" + artist.name +
                        "\" проигнорирован как не существующий.",
                    'type': 'warning'
                });
                $scope.artists = _($scope.artists)
                    .without(artist);
            }
            return;
        }
        if (response.artist.name != artist.name)
        {
            $rootScope.log(
            {
                'message': "Название исполнителя \"" + artist.name + "\" исправлено в пользу \"" +
                    response.artist.name + "\".",
                'type': 'warning'
            });
            var correctArtist = _($scope.artists)
                .find(function (a)
                {
                    return a.name == response.artist.name;
                });
            if (correctArtist)
            {
                $scope.artists = _($scope.artists)
                    .without(artist);
                //console.$rootScope.log('Исправленный встречался!');
                correctArtist.weight += artist.weight;
                if (!_(correctArtist)
                    .has('url'))
                {
                    //console.$rootScope.log('Исправленный корректен!');
                    artist = correctArtist;
                }
                else
                {
                    return;
                }
            }
            else
            {
                //console.$rootScope.log('Исправленный не встречался...');
                artist.name = response.artist.name;
            }
        }
        var numOfListeners = parseInt(response.artist.stats.listeners);
        if (numOfListeners < $scope.LISTENERSLIMIT)
        {
            $scope.artists = _($scope.artists)
                .without(artist);
            $rootScope.log(
            {
                'message': "Исполнитель \"" + artist.name + '" проигнорирован как сомнительный.',
                'type': 'warning'
            });
        }
        else
        {
            //console.log(_(response.artist).keys());
            //_(artist)
            //    .extend(response.artist);
            artist.name = response.artist.name;
            artist.tags = response.artist.tags.tag;
            artist.image = _(response.artist.image)
                .findWhere(
                {
                    'size': 'mega'
                })['#text'];
            if (numOfListeners < 10000)
            {
                artist.isUnderground = true;
            }
            else
            {
                if (numOfListeners > 1000000)
                {
                    artist.isMainstream = true;
                }
            }
            $rootScope.log(
            {
                'message': 'Исполнитель "' + artist.name + '" успешно обработан.',
                'type': 'success'
            });
        }
    };
    var saveArtists = function ()
    {
        //console.log('saveArtists()');
        $rootScope.completeRequestsNum++;
        if (!$scope.audios || !$scope.audios.length) return;
        $scope.audios = _($scope.audios)
            .sortBy(function (audio)
            {
                return audio.artist;
            });
        _($scope.audios)
            .forEach(function (audio)
            {
                if (audio.artist == undefined)
                {
                    return;
                }
                audio.artist = audio.artist.trim();
                var artist = _($scope.artists)
                    .find(function (artist)
                    {
                        return artist.name == audio.artist;
                    });
                if (artist)
                {
                    artist.weight += audio.weight;
                }
                else
                {
                    $scope.artists.push(
                    {
                        'name': audio.artist,
                        'weight': audio.weight
                    });
                }
            });
        audiosNum = $scope.audios.length;
        $scope.audios = undefined;
        //		window.totalRequestsNum+=$scope.artists.length;
    };
    var saveAudios = function (response)
    {
        //console.log('saveAudios('+response+')');
        $rootScope.completeRequestsNum++;
        //$scope.response = response;
        //якщо відповіді нема, пустий масив або 0 єдиний елемент масиву
        if (response.response === undefined || response.response.length == 0 || response.response[0] === 0)
        {
            $scope.artistsString = "<span class='label label-danger'>Аудио нет!</span>";
            $scope.level = 1;
            $rootScope.log(
            {
                'message': "У цели нет аудио",
                'type': 'danger'
            });
            $rootScope.busy = false;
            return $q.reject();
        }
        else
        {
            $scope.audios = response.response;
            //window.numOfAudios = response.response.length;
            _($scope.audios)
                .forEach(function (audio, i, audios)
                {
                    //console.log(audio, i, audios);
                    if (i != 0) audio.weight = audios.length - i;
                });
            return $q.when();
        }
    };
    var saveTags = function ()
    {
        var foundTag;
        _($scope.artists)
            .forEach(function (artist, i, artists)
            {
                window.artist = artist;
                _(artist.tags)
                    .forEach(function (tag, i, tags)
                    {
                        foundTag = _($scope.tags)
                            .findWhere(
                            {
                                'name': tag.name
                            });
                        if (foundTag)
                        {
                            foundTag.weight += artist.weight;
                        }
                        else
                        {
                            $scope.tags.push(
                            {
                                'name': tag.name,
                                'weight': artist.weight
                            });
                        }
                    });
            });
        //window.tags=$scope.tags;
        $scope.tags = _($scope.tags)
            .filter(function (tag)
            {
                return tag.name != 'seen live';
            });
        sortTags();
    };
    var saveUid = function (response)
    {
        //console.log('saveUid('+response+')');
        $rootScope.completeRequestsNum++;
        //window.response = response;
        if (response.error)
        {
            $scope.targetError = "Цель не найдена.";
            $rootScope.log(
            {
                'message': "Цель не найдена",
                'type': 'danger'
            });
            //$rootScope.busy=false;
            $rootScope.$apply();
            $scope.$apply();
            return $q.reject();
        }
        //console.$rootScope.log(response);
        _($scope)
            .extend(response.response[0]);
        $scope.targetError = "";
        $rootScope.log(
        {
            'message': 'Цель - ' + $scope.first_name + ' ' + $scope.last_name + ': принято.',
            'type': 'success'
        });
        if (!$scope.can_see_audio)
        {
            $scope.targetError = "Аудио недоступны.";
            $rootScope.log(
            {
                'message': "Аудио недоступны",
                'type': 'danger'
            });
        }
        $rootScope.appStatus = '';
        $rootScope.busy = false;
        $rootScope.$apply();
        $scope.$apply();
        return $q.defer();
    };
    var screenName = undefined;
    var setBgImage = function ()
    {
        if ($scope.artists)
        {
            console.log($scope.artists[0].image);
            $scope.bgImage = $scope.artists[0].image;
        }
    };
    var showArtists = function ()
    {
        //console.log('showArtists()');
        $scope.artistsString = _.chain($scope.artists)
            .slice(0, $scope.ARTISTSLIMIT)
            .reduce(function (memo, artist, i)
            {
                if (artist.isUnderground)
                {
                    return memo + (
                            (i === 0) ? '' : ', ') + "<span class='underground' title='underground'>" +
                        artist.name + "</span>";
                }
                else
                {
                    if (artist.isMainstream)
                    {
                        return memo + (
                                (i === 0) ? '' : ', ') + "<span class='mainstream' title='mainstream'>" +
                            artist.name + "</span>";
                    }
                    else
                    {
                        return memo + (
                            (i === 0) ? '' : ', ') + "<span>" + artist.name + "</span>";
                    }
                }
            }, '')
            .value();
        //$scope.$container.find('.underground,.mainstream').tooltip();
        //log('Исполнители предоставлены');
    }
    var showTags = function ()
    {
        //console.log('showTags()');
        //теги
        $scope.tagsString = _.chain($scope.tags)
            .slice(0, $scope.TAGSNUMTOSHOW)
            .pluck('name')
            .filter(function (tagName)
            {
                for (var i = 0; i < $scope.filterFromTags.length; i++)
                {
					console.log('looking for '+$scope.filterFromTags[i]+' in '+tagName);
                    if (tagName.indexOf($scope.filterFromTags[i]) !== -1) return false;
                }
                return true;
            })
            .value()
            .join(', ');
        $rootScope.appStatus = '';
        //showArtists();
    };
    var sortArtists = function ()
    {
        //console.log('sortArtists()');
        $scope.artists = _($scope.artists)
            .sortBy(function (artist)
            {
                return -artist.weight;
            });
    };
    var sortTags = function ()
    {
        //console.log('sortArtists()');
        $scope.tags = _($scope.tags)
            .sortBy(function (tag)
            {
                return -tag.weight;
            });
    };
    $scope.targetError = "";
    if (!$scope.targets.length)
    {
        $scope.inputId = $rootScope.userId;
        $scope.resolveUid();
    }
    $scope.targets.push($scope);
    //console.log($scope.avto);
});
