"use strict";
socmusrecApp.controller('LoginCtrl', function LoginCtrl($scope, $location, $rootScope)
{
    VK.init(
    {
        'apiId': $rootScope.vkApiId
    });
    $scope.login = function ()
    {
        //console.log('$rootScope.login()');
        $rootScope.totalRequestsNum++;
        VK.Auth.login($scope.checkAuthInfo, 8);
    };
    $scope.checkAuthInfo = function (response)
    {
        $rootScope.completeRequestsNum++;
        //console.log(response);
        if (response.session)
        {
            //console.log(response.session);
            //отримати масив параметрів URI
            //var paramz = window.location.hash.substr(1)
            //    .split('&');
            //ввести у #target-1 .target-id id поточного користувача
            //window.userId = response.session.mid;
            $location.path('/comparison');
            $rootScope.userId = response.session.mid;
            //$rootScope.targets[0].resolveLink();
            $scope.$apply();
        }
    }
});
