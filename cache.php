<?php
	$json = file_get_contents('php://input');
	$data = json_decode($json, true);

	foreach($data as $val) {
		$file = 'cache/'.$val['uid'].'.json';
		$content = json_encode($val);

		file_put_contents($file, $content);
	}
?>
