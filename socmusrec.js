"use strict";
var socmusrecApp=angular.module('socmusrecApp', [])
	.run(function($rootScope, $q){
		//масив, щоб зберігати муз. виконавців
		$rootScope.targets = [];
		$rootScope.lastfmKey = "2dcdc793309afa969dffb976eb7702d1";
		//Алфавіт в капсі
		$rootScope.alphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		//Алфавіт не в капсі
		$rootScope.alphaLower = $rootScope.alphaUpper.toLowerCase();
		$rootScope.areTargetsPresent=function(){
			console.log('$rootScope.areTargetsPresent()');
			return $rootScope.targets[0].can_see_audio && $rootScope.targets[1].can_see_audio;
		};
		//Максимальна оброблювана кількість виконавців
		$rootScope.ARTISTSLIMIT = 50;

		$rootScope.checkAuthInfo=function(response)
		{
			console.log('$rootScope.checkAuthInfo('+response+')');
			if (response.session)
			{
				//отримати масив параметрів URI
				var paramz = window.location.hash.substr(1)
					.split('&');
				//ввести у #target-1 .target-id id поточного користувача
				window.userId=response.session.mid;
				$rootScope.targets[0].inputId=$rootScope.userId=response.session.mid;
				//$rootScope.targets[0].resolveLink();
				$rootScope.targets[0].resolveUid();
			}
		}
		$rootScope.compare=function()
		{
			console.log('$rootScope.compare()');
			$rootScope.log('Сравнение вкусов');
			if (!$rootScope.isReady())
			{
				$rootScope.log('Цели сравнения не готовы!');
				var result=$q.defer();
				result.reject();
				return result;
			}
			var similarity = 0.0;
			var commonTags = Object.create(null);
			//var tags1Sum=$rootScope.targets[0].getTagsWeightSum();
			var tags1RelWeights=$rootScope.targets[0].getTagsRelWeights();
			//var tags2Sum=$rootScope.targets[1].getTagsWeightSum();
			var tags2RelWeights = $rootScope.targets[1].getTagsRelWeights();
			_(tags1RelWeights)
				.forEach(function(relWeight, tag)
				{
					if ( tag in tags2RelWeights)
					{
						commonTags[tag] = (relWeight < tags2RelWeights[tag]) ? relWeight : tags2RelWeights[tag];
						similarity += commonTags[tag];
					}
				});
			$rootScope.commonTags = commonTags;
			var commonTagsString = _.chain(commonTags)
				.pairs()
				.sortBy(function(tagPair)
				{
					return -tagPair[1];
				})
				.map(function(tagPair)
				{
					return tagPair[0];
				})
				.slice(0, $rootScope.TAGCOUNTLIMIT)
				.reduce(function(memo, tag, i)
				{
					return memo + ((i === 0) ? '' : ', ') + tag;
				}, '')
				.value();
			$rootScope.similarity={
				'description':commonTagsString?commonTagsString:'...',
				'quantity':Math.trunc(similarity)
			};
		};
		$rootScope.completeRequestsNum=0;
		$rootScope.isReady=function(){
			console.log('$rootScope.isReady()');
			return $rootScope.targets[0].isReady()&&$rootScope.targets[1].isReady();
		};
		$rootScope.status='';
		$rootScope.LISTENERSLIMIT = 100;
		$rootScope.log=function(message){
			console.log(message);
			$rootScope.status=message;
		};
		$rootScope.login=function(){
			console.log('$rootScope.login()');
			VK.Auth.login($rootScope.checkAuthInfo, 8);
		};
		$rootScope.similarity=null;
		//кількість подібних виконавців, що обробляються для кожного виконавця
		$rootScope.SIMILARPERARTIST = 10;
		$rootScope.startComparison=function(){
			console.log('$rootScope.startComparison()');
			if(!$rootScope.targets[0].uid||!$rootScope.targets[1].uid)
			{
				$rootScope.log("Один из ID пустой!");
				var result=$q.defer();
				result.reject();
				return result;
			}
			$rootScope.totalRequestsNum=2;
			$rootScope.completeRequestsNum=0;
			var task1;
			if(!$rootScope.targets[0].tags||$rootScope.targets[0].tags.length<1)
				task1=$rootScope.targets[0].loadEverything();
			else task1=$q.defer();
			var task2;
			if(!$rootScope.targets[1].tags||$rootScope.targets[1].tags.length<1)
				task2=$rootScope.targets[1].loadEverything();
			else
				task2=$q.defer();
			return $q.all([task1,task2])
				.then($rootScope.compare);
		};
		$rootScope.status="";
		//кількість тегів, що обробляються для кожного виконавця
		$rootScope.TAGCOUNTLIMIT = 5;
		//кількість улюблених тегів, які слід показати
		$rootScope.TAGSNUMTOSHOW = 10;
		$rootScope.totalRequestsNum=0;
		$rootScope.userId=0;
		$rootScope.vkApiId = 4988609;
		//дивні відповіді
		//$rootScope.strange = [];
		VK.init(
		{
			'apiId' : $rootScope.vkApiId
		});
	});
socmusrecApp.controller('ComparisonCtrl',function ComparisonCtrl($scope, $location) {
//дивні відповіді
//$rootScope.strange = [];
});
socmusrecApp.controller('TargetCtrl',function TargetCtrl($scope, $rootScope, $http, $q, $timeout) {
	$scope.artists=[];
	$scope.busy=false;
	$scope.correctArtist=function(response){
		console.log('$scope.correctArtist('+response+')');
		$rootScope.log("Корректировка исполнителя " + artist.name);
		if (response.error)
		{
			if (response.error == 6)
			{
				$rootScope.log("Исполнитель \"" + artist.name + "\" не существует.")
				$scope.artists = _($scope.artists)
					.without(artist);
			}
			return;
		}
		if (response.artist.name != artist.name)
		{
			$rootScope.log(artist.name + " исправляется в " + response.artist.name);
			var correctArtist = _($scope.artists)
				.find(function(a)
				{
					return a.name == response.artist.name;
				});
			if (correctArtist)
			{
				$scope.artists = _($scope.artists)
					.without(artist);
				//console.$rootScope.log('Исправленный встречался!');
				correctArtist.weight += artist.weight;
				if (!_(correctArtist)
					.has('url'))
					{
						//console.$rootScope.log('Исправленный корректен!');
						artist = correctArtist;
					} else
					{
						return;
					}
			} else
			{
				//console.$rootScope.log('Исправленный не встречался...');
				artist.name = response.artist.name;
			}
		}
		var numOfListeners = parseInt(response.artist.stats.listeners);
		if (numOfListeners < $rootScope.LISTENERSLIMIT)
		{
			$rootScope.log("Исполнитель \"" + artist.name + '" сомнителен.');
			$scope.artists = _($scope.artists)
				.without(artist);
		} else
		{
			$rootScope.log(artist.name + ' - OK!');
			_(artist)
				.extend(response.artist);
			if (numOfListeners < 10000)
			{
				artist.isUnderground = true;
			} else
			{
				if (numOfListeners > 1000000)
				{
					artist.isMainstream = true;
				}
			}
			$scope.numOfCorrectArtists++;
		}
	};
	$scope.getTagsRelWeights=function()	{
		console.log('$scope.getTagsRelWeights()');
		var tagsRelWeights=Object.create(null);
		_($rootScope.targets[0].tags)
			.forEach(function(weight, tag, tags)
			{
				tagsRelWeights[tag] = weight * 100 / $scope.getTagsWeightSum();
			});
		return tagsRelWeights;
	};
	$scope.getTagsWeightSum=function(){
		console.log('$scope.getTagsWeightSum()');
		return _($scope.tags)
			.values()
			.reduce(function(memo, weight){
				return memo + weight;
			}, 0);
	};
	$scope.handleInput=function(){
		console.log('$scope.handleInput()');
		$scope.inputQs.push($timeout(function(){},1000));
		if(!$scope.isReceivingInput)
		{
			$scope.isReceivingInput=true;
			$q.all($scope.inputQs)
				.then(function(){
					$scope.isReceivingInput=false;
					$scope.inputQs=[];
					$scope.resolveUid();
				});
		}
	};
	$scope.inputId="";
	$scope.inputQs=[];
	$scope.isReady=function(){
		console.log('$scope.isReady()');
		return $scope.can_see_audio && $scope.tags && $scope.tags.length>0;
	};
	$scope.isReceivingInput=false;
	$scope.linkRe = /(?:(?:https?:)?\/\/(?:m\.)?vk\.com\/)?([^\?#]*)(?:\?.*)?(?:#.*)?/i;
	$scope.loadAudios=function(){
		console.log('$scope.loadAudios()');
		$rootScope.log("Отправка запроса");
		//$scope.busy=true;
		var deferred=$q.defer();
		window.deferred=deferred;
		console.log($scope.uid);
		console.log("VK.Api.call('audio.get',{'owner_id' : "+$scope.uid+"}, "+deferred.resolve+")");
		VK.Api.call("audio.get",
		{
			'owner_id' : $scope.uid
		}, deferred.resolve);
		return deferred.promise;
	};
	$scope.loadEverything=function(){
		console.log('$scope.loadEverything()');
		return $scope.loadAudios()
			.then($scope.saveAudios,function(err){console.log('err! '+err);})
			.then($scope.saveArtists)
			.then($scope.requestArtistCorrections)
			.then($scope.loadTags)
			.then($scope.showTags);
	};
	$scope.loadTags = function(){
		console.log('$scope.loadTags()');
		//setOperation('Получение тегов');
		//$scope.blockInput();
		$scope.tags = Object.create(null);
		var qs = [];
		_.chain($scope.artists)
		//Отримання зрізу масиву від 0 до $rootScope.targets[0].artistsLIMIT елемента не
		// включно
		/*.slice(0, $rootScope.ARTISTSLIMIT)*/.forEach(function(artist, i)
		{
			$rootScope.log('Запрос: ' + artist.name);
			$rootScope.totalRequestsNum+=1;
			
			//додати до масиву відкладених надісланий ajax-запит
			qs.push($http({
				'method':'GET',
				'url' : 'https://ws.audioscrobbler.com/2.0/',
				'params': {
					'api_key' : $rootScope.lastfmKey,
					'artist' : artist.name,
					'autocorrect' : 0,
					'format' : 'json',
					'method' : 'artist.gettoptags'
				}
			})
			//додати до ланцюжка і повернути новий відкладений
			.then($scope.saveArtistTags));
		});
		//повернути відкладення, що завершиться після завершення qs (котрі усі уже
		// запущені)
		return $q.all(qs);
	}
	$scope.requestArtistCorrection=function(artist){
		console.log('$scope.requestArtistCorrection()');
		$http({
			'method':'GET',
			'url':'https://ws.audioscrobbler.com/2.0/',
			params: {
				'api_key' : $rootScope.lastfmKey,
				'artist' : artist.name,
				'autocorrect' : 1,
				'format' : 'json',
				'method' : 'artist.getinfo'
			}
		})
			.then($scope.correctArtist);
	};
	$scope.requestArtistCorrections=function(){
		console.log('$scope.requestArtistCorrections()');
		if ($scope.numOfCorrectArtists === undefined)
			$scope.numOfCorrectArtists = 0;
		var qs = [];
		if ($scope.numOfCorrectArtists < $rootScope.ARTISTSLIMIT && _($scope.artists).size() > $scope.numOfCorrectArtists)
		{
			_.chain($scope.artists)
				.slice($scope.numOfCorrectArtists, $rootScope.ARTISTSLIMIT)
				.forEach(function(artist)
				//_.chain($scope.artists).forEach(function(weight,artist)
				{
					qs.push($scope.correctArtist(artist));
				});

			return $q.all(qs)
				.then(function()
				{
					$scope.sortArtists();
					$rootScope.status='';
				})
				.then($scope.requestArtistCorrections);
		}
	};
	$scope.reset=function(){
		console.log('$scope.reset()');
		$scope.artists = [];
		$scope.uid=undefined;
		$scope.tags = null;
		$scope.numOfCorrectArtists = 0;
		$scope.busy=false;
	};
	$scope.resolveUid = function()
	{
		console.log('$scope.resolveUid()');
		//$scope.$apply();
		$rootScope.log('Пытаемся получить пользователя...');
		var ololo = $scope.linkRe.exec($scope.inputId);
		if (!ololo[1] || ololo[1] == $scope.screenName)
		{
			return;
		}
		$scope.screenName = ololo[1];
		$scope.reset();
		VK.Api.call('users.get',
		{
			'user_ids' : $scope.screenName,
			'fields' : 'can_see_audio,photo_50'
		}, $scope.saveUid);
	};
	$scope.saveArtists = function()
	{
		console.log('$scope.saveArtists()');
		if (!$scope.audios || !$scope.audios.length)
			return;
		$scope.audios = _($scope.audios)
			.sortBy(function(audio)
			{
				return audio.artist;
			});
		_($scope.audios)
			.forEach(function(audio)
			{
				if (audio.artist == undefined)
				{
					return;
				}
				audio.artist = audio.artist.trim();
				var artist = _($scope.artists)
					.find(function(artist)
					{
						return artist.name == audio.artist;
					});
				if (artist)
				{
					artist.weight += audio.weight;
				} else
				{
					$scope.artists.push(
					{
						'name' : audio.artist,
						'weight' : audio.weight
					});
				}
			});
//		window.totalRequestsNum+=$scope.artists.length;
		$scope.sortArtists();
	};
	$scope.saveArtistTags=function(response)
	{
		console.log('$scope.saveArtistTags('+response+')');
		$rootScope.completeRequestsNum+=1;
		
		$rootScope.log('Обработка: ' + artist.name);
		//якщо у JSON-відповіді є ключ toptags.#text
		if (response.error || response.toptags['#text'])
		{
				//$rootScope.strange.push(response);
			return;
		}
		//Обгорнути response.toptags.tag в underscore-об’єкт
		_(response.toptags.tag)
		//знайти перший елемент, для якого передана функція поверне true (насправді обхід
		// зрізу масива до елемента номер $rootScope.TAGCOUNTLIMIT)
			.find(function(tag)
			{
				if (!tag.name || tag['#text'] || tag.name == 'seen live')
				{
					//$rootScope.strange.push(response);
					return;
				}
				if (parseInt(tag.count) < $rootScope.TAGCOUNTLIMIT)
				{
					//console.$rootScope.log('tag ' + tag.name + ' is oops.');
					return true;
				}
				//якщо тег уже зустрічався
				if (tag.name in $scope.tags)
				{
					$scope.tags[tag.name] += tag.count * artist.weight;
				} else
				{
					//вага тега дорівнює count, помноженому на вагу виконавця
					$scope.tags[tag.name] = tag.count * artist.weight;
				}
				return false;
			});
	};
	$scope.saveAudios = function(response)
	{
		console.log('$scope.saveAudios('+response+')');
		window.completeRequestsNum+=1;
		$rootScope.log('Обработка аудио');
		//$scope.response = response;
		//якщо відповіді нема, пустий масив або 0 єдиний елемент масиву
		if (response.response === undefined || response.response.length == 0 || response.response[0] === 0)
		{
			$scope.artistsString="Аудио нет!";
			return $q.reject();
		} else
		{
			$scope.audios = response.response;
			//window.numOfAudios = response.response.length;
			_($scope.audios)
				.forEach(function(audio, i, audios)
				{
					audio.weight = audios.length - i;
				});
			return $q.when();
		}
	};
	$scope.saveUid=function(response){
		console.log('$scope.saveUid('+response+')');
		window.response=response;
		if (response.error)
		{
			$scope.targetError="Цель не найдена.";
			$rootScope.$apply();
			$scope.$apply();
			$rootScope.log(response.error);
			return $q.reject();
		}
		//console.$rootScope.log(response);
		_($scope)
			.extend(response.response[0]);
		$scope.targetError="";
/*		var targetInfoString = '<a href="//vk.com/id' + $scope.uid + '" target="__blank"><img src="' + $scope.photo_50 + '"/>' + $scope.first_name + ' ' + $scope.last_name + '</a>' + ($scope.can_see_audio ? '' : '. Аудио недоступны.');*/
		/*var session = VK.Auth.getSession();
		if (session && $scope.uid == session.mid)
		{
			$('.user-info').html(userInfo);
		}*/
/*		$scope.targetInfo=targetInfoString;*/
		$rootScope.status='';
		$rootScope.$apply();
		$scope.$apply();
		return $q.defer();
	};
	$scope.screenName=undefined;
	$scope.showTags=function()
	{
		console.log('$scope.showTags()');
		$rootScope.log('Сортировка и вывод тегов');
		//теги
		var tagPairs = _.chain($scope.tags)
			.pairs()
		/*.filter(function(tagPair)
		{
		return tagPair[1] > 1.0;
		})*/
		//відсортувати за спаданням ваги
			.sortBy(function(tagPair)
			{
				return -tagPair[1];
			})
			.value();
		$scope.tags = _(tagPairs)
			.object();
		$scope.tagsString = _(tagPairs)
			.slice(0, $rootScope.TAGSNUMTOSHOW)
			.reduce(function(memo, tagPair, i)
			{
				return memo + ((i == 0) ? '' : ', ') + tagPair[0];
			}, '');
		$rootScope.status='';
		//$scope.showArtists();
	};
	$scope.sortArtists = function()
	{
		console.log('$scope.sortArtists()');
		$scope.artists = _($scope.artists)
			.sortBy(function(artist)
			{
				return -artist.weight;
			});
	};
	$scope.targetError="";
	var dfrd=$q.defer();
	dfrd=dfrd.promise.then(function(){
		alert($scope.testVar);
	});
	$scope.test=function(){
		if(!$scope.testVar)
			testVar="ololo";
		else
		{
			dfrd.resolve();
		}
	};
	$rootScope.targets.push($scope);
});
